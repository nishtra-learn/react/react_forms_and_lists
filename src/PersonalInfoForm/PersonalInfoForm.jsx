import React from 'react';

export default class PersonalInfoForm extends React.Component {
    static defaults = {
        name: 'Nishtra',
        phoneNumber: '+38097123456789',
        email: 'id.yuuko@gmail.com',
        address: 'Kryvyi Rih, Ukraine'
    }

    state = {
        name: PersonalInfoForm.defaults.name,
        phoneNumber: PersonalInfoForm.defaults.phoneNumber,
        email: PersonalInfoForm.defaults.email,
        address: PersonalInfoForm.defaults.address
    }

    constructor() {
        super();

        this.handleNameInputChange = this.handleNameInputChange.bind(this);
        this.handlePhoneInputChange = this.handlePhoneInputChange.bind(this);
        this.handleEmailInputChange = this.handleEmailInputChange.bind(this);
        this.handleAddressInputChange = this.handleAddressInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resetDefaults = this.resetDefaults.bind(this);
    }

    handleNameInputChange(e) {
        this.setState({ name: e.target.value });
    }
    handlePhoneInputChange(e) {
        this.setState({ phoneNumber: e.target.value });
    }
    handleEmailInputChange(e) {
        this.setState({ email: e.target.value });
    }
    handleAddressInputChange(e) {
        this.setState({ address: e.target.value });
    }
    handleSubmit(e) {
        e.preventDefault();

        let msg = `Name: ${this.state.name}` +
            `\nPhone: ${this.state.phoneNumber}` +
            `\nEmail: ${this.state.email}` +
            `\nAddress: ${this.state.address}`;
        alert(msg);
    }
    resetDefaults() {
        this.setState(PersonalInfoForm.defaults);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <table>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td><input type="text" value={this.state.name} onChange={this.handleNameInputChange} /></td>
                        </tr>
                        <tr>
                            <td>Phone number</td>
                            <td><input type="text" value={this.state.phoneNumber} onChange={this.handlePhoneInputChange} /></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type="text" value={this.state.email} onChange={this.handleEmailInputChange} /></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><input type="text" value={this.state.address} onChange={this.handleAddressInputChange} /></td>
                        </tr>
                    </tbody>
                </table>
                <input type="submit" value="Submit" />
                <input type="button" value="Reset defaults" onClick={this.resetDefaults}/>
            </form>
        )
    }
}