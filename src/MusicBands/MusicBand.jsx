import React from 'react';

export default class MusicBand extends React.Component {

    render() {
        let band = this.props.data;

        return (
            <div className="MusicBand">
                <table>
                    <tbody>
                        <tr>
                            <td colSpan="2"><h3>{band.name}</h3></td>
                        </tr>
                        <tr>
                            <td>Genres </td>
                            <td>
                                {
                                    band.genres.join(', ')
                                }
                            </td>
                        </tr>
                        <tr>
                            <td>Members </td>
                            <td>
                                <ul>
                                    {
                                        band.members.map(member => {
                                            return <li key={member}>{member}</li>
                                        })
                                    }
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        )
    }
}