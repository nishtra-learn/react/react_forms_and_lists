export const BANDS_LIST = [
    {
        name: 'Unlucky Morpheus',
        genres: [
            'Technical Melodic Speed Metal', 'Symphonic Metal', 'Power Metal'
        ],
        members: [
            'Shiren (Guitar, harsh vocals, composition, arrangement, programming)',
            'Fuki (Vocals, chorus, lyrics)',
            'Jinya (Guitar)',
            'Ogawa Hiroyuki (Bass)',
            'FUMIYA (Drums)',
            'Jill (Violin)'
        ]
    },
    {
        name: 'Galneryus',
        genres: [
            'Power metal', 'Symphonic metal', 'Neoclassical metal'
        ],
        members: [
            'Syu – guitar, backing vocals, leader (2001–present)',
            'Yuhki – keyboards, Hammond organ, keytar, backing vocals (2003–present, support in 2002)',
            'Masatoshi "Sho" Ono (小野 正利, Ono Masatoshi) – vocals (2009–present)',
            'Taka – bass (2009–present)',
            'Lea – drums (2020–present)'
        ]
    },
    {
        name: 'Wagakki Band',
        genres: [
            'Folk rock', 'J-pop', 'metal'
        ],
        members: [
            'Yuko Suzuhana (鈴華ゆう子) – vocals',
            'Machiya (町屋) – guitar',
            'Beni Ninigawa (蜷川べに) – tsugaru shamisen',
            'Kiyoshi Ibukuro (いぶくろ聖志) – koto',
            'Asa (亜沙) – bass',
            'Daisuke Kaminaga (神永大輔) – shakuhachi',
            'Wasabi (山葵) – drums',
            'Kurona (黒流) – wadaiko'
        ]
    },
    {
        name: 'Poets of the Fall',
        genres: ['Progressive Rock'],
        members: [
            'Marko Saaresto (lead vocals)',
            'Olli Tukiainen (lead guitar)',
            'Markus "Captain" Kaarlonen (keyboards, production)',
            'Jani Snellman (bass)',
            'Jaska Mäkinen (rhythm guitar)',
            'Jari Salminen (drums, percussion)'
        ]
    },
];