import React from 'react';
import MusicBand from './MusicBand';
import './MusicBandList.css';

export default class MusicBandList extends React.Component {
    render() {
        let musicBands = this.props.data;

        return (
            <div className="MusicBandList">
                <ul>
                    {
                        musicBands.map(band => <MusicBand data={band}></MusicBand>)
                    }
                </ul>
            </div>
        )
    }
}