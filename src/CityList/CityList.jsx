import React from 'react';
import City from './City';

export default function CityList(props) {
    let cities = props.data;

    return (
        <div className="CityList">
            <ul>
                {
                    cities.map(city => {
                        return <li><City data={city}></City></li>
                    })
                }
            </ul>
        </div>
    )
}