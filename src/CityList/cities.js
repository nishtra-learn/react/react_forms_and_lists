export let list_of_cities = [
    { name: 'Киев', foundationYear: 'V-X век', populace: 2967360 },
    { name: 'Кривой Рог', foundationYear: 1775, populace: 624579 },
    { name: 'Запорожье', foundationYear: 952, populace: 731922 },
    { name: 'Днепр', foundationYear: 1776, populace: 999725 },
    { name: 'Кременчуг', foundationYear: 1571, populace: 218553 },
    { name: 'Полтава', foundationYear: 899, populace: 284519 },
    { name: 'Одесса', foundationYear: 1794, populace: 1017699 },
    { name: 'Николаев', foundationYear: 1789, populace: 477911 }
]