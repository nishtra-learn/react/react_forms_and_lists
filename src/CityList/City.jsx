import React from 'react';

export default function City(props) {
    let data = props.data;
    let populaceDisplayValue = data.populace;
    if (data.populace >= 1000000)
        populaceDisplayValue = (data.populace / 1000000).toFixed(2) + ' млн.';
    else if (data.populace >= 1000)
        populaceDisplayValue = (data.populace / 1000).toFixed(2) + ' тыс.';

    return (
        <div>{data.name} (основан {data.foundationYear}), население {populaceDisplayValue}</div>
    )
}