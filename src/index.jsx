import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import PersonalInfoForm from './PersonalInfoForm/PersonalInfoForm';
import CityList from './CityList/CityList';
import { list_of_cities } from './CityList/cities';
import RegistrationForm from './RegistrationForm/RegistrationForm';
import MusicBandList from './MusicBands/MusicBandList';
import { BANDS_LIST } from './MusicBands/bandsList'

ReactDOM.render(
  <React.StrictMode>
    <h3>PersonalInfoForm</h3>
    <PersonalInfoForm></PersonalInfoForm>
    <hr />

    <h3>CityList</h3>
    <CityList data={list_of_cities}></CityList>
    <hr />

    <h3>RegistrationForm</h3>
    <RegistrationForm></RegistrationForm>
    <hr />

    <h3>MusicBands</h3>
    <MusicBandList data={BANDS_LIST}></MusicBandList>
    <hr />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
