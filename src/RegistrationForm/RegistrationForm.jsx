import React, { useState } from 'react';

export default function RegistrationForm(props) {
    const genderList = ['Male', 'Female', 'Other'];
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [gender, setGender] = useState(genderList[0]);
    const [age, setAge] = useState(0);

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!username || !email || !age) {
            alert('Please fill all the fields');
            return;
        }

        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(email).toLowerCase())) {
            alert('Please enter a valid email address');
            return;
        }

        let msg = `Username: ${username}` +
            `\nEmail: ${email}` +
            `\nGender: ${gender}` +
            `\nAge: ${age}`;
        alert(msg);
    }
    const handleNameInputChange = (e) => {
        let val = e.target.value;

        if (val.length > 30) {
            alert('Username can not be longer than 30 characters');
            return;
        }

        setUsername(val);
    }
    const handleEmailInputChange = (e) => {
        let val = e.target.value;
        setEmail(val);
    }
    const handleGenderSelectChange = (e) => {
        let val = e.target.value;
        setGender(val);
    }
    const handleAgeInputChange = (e) => {
        let val = e.target.value;

        if (val < 0 || val > 150)
        {
            alert('A valid range for age is between 1 and 150')
            return;
        }

        setAge(val);
    }

    return (
        <form onSubmit={handleSubmit}>
            <table>
                <tbody>
                    <tr>
                        <td>Username</td>
                        <td><input type="text" value={username} onChange={handleNameInputChange} /></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><input type="text" value={email} onChange={handleEmailInputChange} /></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td>
                            <select name="" id="" value={gender} onChange={handleGenderSelectChange}>
                                {
                                    genderList.map(gender => <option key={gender}>{gender}</option>)
                                }
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td><input type="text" value={age} onChange={handleAgeInputChange} /></td>
                    </tr>
                </tbody>
            </table>
            <input type="submit" value="Submit" />
        </form>
    )
}